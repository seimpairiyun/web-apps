# F U L L S T A C K / API 

I  decide to learn some web stack to..<br>
Serious: to understand meme out there.<br>
!Serious: to understand every meme out there.

## PHP  
- [ ] [Laravel](https://gitlab.com/seimpairiyun/web-apps/-/tree/main/PHP) 

## Python 
- [ ] [Django](https://gitlab.com/seimpairiyun/web-apps/-/tree/main/Python)
- [ ] [Socketify](https://gitlab.com/seimpairiyun/web-apps/-/tree/main/Python)

## Javascript (Node/Bun)
- [ ] [Express](https://gitlab.com/seimpairiyun/web-apps/-/tree/main/Javascript)
- [ ] [Elysia](https://gitlab.com/seimpairiyun/web-apps/-/tree/main/Javascript)

## GO
- [ ] [Gin](https://gitlab.com/seimpairiyun/web-apps/-/tree/main/Go)
- [ ] [Fiber](https://gitlab.com/seimpairiyun/web-apps/-/tree/main/Go)

## Rust
- [ ] [Axum](https://gitlab.com/seimpairiyun/web-apps/-/tree/main/Rust)
- [ ] [Actix](https://gitlab.com/seimpairiyun/web-apps/-/tree/main/Rust)


